package aed;

public class ListaEnlazada<T> implements Secuencia<T> {
    // Completar atributos privados

    int size;
    private Nodo primero;
    private Nodo ultimo;

    private class Nodo {
        Nodo ant;
        Nodo sig;
        T valor;
    }

    public ListaEnlazada() {
        size = 0;
        Nodo ant = null;
        Nodo sig = null;
    }

    public int longitud() {
        return size;
    }

    public void agregarAdelante(T elem) {
       Nodo nuevo = new Nodo();
       nuevo.valor = elem;
       nuevo.ant = null;

       if(size == 0){
            primero = nuevo;
            ultimo = nuevo;
        }
       else{
            primero.ant = nuevo;
            nuevo.sig = primero;
            primero = nuevo;
       }
       size++;
    }

    public void agregarAtras(T elem) {
        Nodo nuevo = new Nodo();
        nuevo.valor = elem;
        nuevo.ant = null;
        nuevo.sig = null;

        if(size == 0){
            primero = nuevo;
            ultimo = nuevo;
        }
        else {
            ultimo.sig = nuevo;
            nuevo.ant = ultimo;
            ultimo = nuevo;

        }
        size++;
    }

    public T obtener(int i){
        Nodo actual = new Nodo();
        actual = primero;

        int j = 0;
        while(j != i){
            actual = actual.sig;
            j++;
        }
        return actual.valor;
    }

    public void eliminar(int i) {
        Nodo actual = new Nodo(); 
        actual = primero;


        if(i == 0 && size == 1){ // borrar unico elemento
            primero = null;
            ultimo = null;
        }

        if(i == size){ // borrar el ultimo
            ultimo = ultimo.ant;
            ultimo.sig = null;
        }

        if(i == 0 && size > 1){ // borrar el primero
            primero = primero.sig;
            primero.ant = null;
        }

        if (1 <= i && i < size){ // borrar uno entre el segundo y anteultimo elemento
            int j = 0;
            while(j != i - 1){
            actual = actual.sig;
            j++;
            }
            actual.sig = actual.sig.sig;
        }
        size--;
    }

    public void modificarPosicion(int indice, T elem) {
        Nodo actual = new Nodo();
        actual = primero;

        int j = 0;
        while(j != indice){
            actual = actual.sig;
            j++;
        }
        actual.valor = elem;
    }

    public ListaEnlazada<T> copiar() {
        ListaEnlazada<T> copia = new ListaEnlazada();

        Nodo actual = primero;
        while(actual != null){
            copia.agregarAtras(actual.valor);
            actual = actual.sig;
        }

        return copia;
    }

    public ListaEnlazada(ListaEnlazada<T> lista) {
        Nodo actual = lista.primero;
        while(actual != null){
            agregarAtras(actual.valor);
            actual = actual.sig;
        }
    }
    
    @Override
    public String toString() {
        String listaString = "[";
        Nodo actual = primero;

        while(actual != null){

            listaString += actual.valor.toString();

            if (actual == ultimo){
                actual = actual.sig;
            }
            else{
                listaString += ", ";
                actual = actual.sig;  
            }
        }
        listaString += "]";

        return listaString;
    }

    private class ListaIterador implements Iterador<T> {
    	// Completar atributos privados
        int dedito = 0;
        Nodo actualNodo = primero;

        public boolean haySiguiente() {
	        return dedito < size;
        }
        
        public boolean hayAnterior() {
	        return dedito > 0;
        }

        public T siguiente() {
            T valor = obtener(dedito);
            dedito = dedito + 1;
            if(actualNodo != null){
                actualNodo = actualNodo.sig;
            }
            return valor;
        }
        

        public T anterior() {
            dedito = dedito - 1;
            T valor = obtener(dedito);
            if(actualNodo != null){
                actualNodo = actualNodo.ant;
            }
            return valor;
        }
    }

    public Iterador<T> iterador(){
        ListaIterador it = new ListaIterador();
        return it;
    }
}