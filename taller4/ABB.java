package aed;

import java.util.*;


// Todos los tipos de datos "Comparables" tienen el método compareTo()
// elem1.compareTo(elem2) devuelve un entero. Si es mayor a 0, entonces elem1 > elem2
public class ABB<T extends Comparable<T>> implements Conjunto<T> {
    // Agregar atributos privados del Conjunto
    Nodo raiz;
    int size = 0;

    private class Nodo {
        // Agregar atributos privados del Nodo
        T valor;
        Nodo menor;
        Nodo mayor;
        // Crear Constructor del nodo
        Nodo(T elem){
            this.valor = elem;
            this.mayor = null;
            this.menor = null;
        }
    }

    public ABB() {
        raiz = new Nodo(null);
        size = 0;
    }

    public int cardinal() {
        return size;
    }

    public T minimo(){
        Nodo actual = raiz;

        if(size == 0){
            return raiz.valor;
        }

        while(actual.menor != null){
            actual = actual.menor;
        }
        return actual.valor;
    }

    public T maximo(){
        Nodo actual = raiz;

        if(size == 0){
            return raiz.valor;
        }

        while(actual.mayor != null){
            actual = actual.mayor;
        }
        return actual.valor;
    }

    public Nodo buscar_nodo(Nodo raiz, T elem){
        if(raiz == null){
            return raiz;
        }

        if (raiz.valor.compareTo(elem) == 0) {
            return raiz;
        }
        if(raiz.valor.compareTo(elem) > 0 ){
            if(raiz.menor != null){
                return buscar_nodo(raiz.menor, elem);
            }else{
                return raiz;
            }
        }
        else{
            if(raiz.mayor != null){
                return buscar_nodo(raiz.mayor, elem);
            }else{
                return raiz;
            }
        }
        
    }

    public void insertar(T elem){
        Nodo actual = raiz;

        if(size ==  0){
            raiz = new Nodo(elem);
            size++;
        }
        else{
            Nodo ultiNodo = buscar_nodo(raiz, elem);
            if(ultiNodo.valor.compareTo(elem) != 0){
                
                while(actual.valor.compareTo(ultiNodo.valor) != 0){
                    if(actual.valor.compareTo(elem) > 0){
                        actual = actual.menor;
                    }
                    else{
                        actual = actual.mayor;
                    }
                }
                if(actual.valor.compareTo(elem) > 0){
                    actual.menor = new Nodo(elem);
                }
                else{
                    actual.mayor = new Nodo(elem);
                }
                size++;
            }

        }

    }

    private boolean busqueda_recursiva(Nodo actual, T valor) {
        if (actual == null || actual.valor == null) {
            return false;
        } 
        if (valor.compareTo(actual.valor) == 0) {
            return true;
        } 
        if(valor.compareTo(actual.valor) > 0){
            return busqueda_recursiva(actual.mayor, valor);
        }
        else{
            return busqueda_recursiva(actual.menor, valor);
        }

    }

    public boolean pertenece(T elem){
        return busqueda_recursiva(raiz,elem);
    }

    public T sucesor(Nodo actual){

        Nodo PadreSucc = actual;
        if(actual == null){
            return null;
        }

        Nodo succ = actual.mayor; // Busco desde el mayor del actual el minimo

        while (succ.menor != null) {
            PadreSucc = succ;
            succ = succ.menor;
        }

        if(PadreSucc.valor.compareTo(actual.valor) != 0) // le asigno al menor el mayor
            PadreSucc.menor = succ.mayor;
        else // si no tiene sucesor le asigno al mayor
            PadreSucc.mayor = succ.mayor;

        return succ.valor; // devuelve el valor del sucesor
    }

    public void eliminar(T elem){

        if(busqueda_recursiva(raiz, elem)){ // Si el valor a eliminar existe

            Nodo ultiNodo = buscar_nodo(raiz, elem);
            Nodo actual = raiz; // apunta al nodo a eliminar
            Nodo padre = null; // padre de el nodo a eliminar

            while(actual.valor.compareTo(ultiNodo.valor) != 0){ // Llegar al valor a eliminar
                padre = actual;
                if(elem.compareTo(actual.valor) < 0){
                    actual = actual.menor;
                }
                else{
                    actual = actual.mayor;
                }
            }
       


            if(actual.menor == null || actual.mayor == null){ // Si tiene un solo hijo

                Nodo nuevoNodo; // el nodo a reemplazar el eliminado
                if(actual.menor == null){
                    nuevoNodo = actual.mayor;
                }
                else{
                    nuevoNodo = actual.menor;
                }

                if(padre == null){ // si el nodo a eliminar es la raiz
                    raiz = nuevoNodo;
                    size--;
                    return;
                }

                if(actual == padre.menor){
                    padre.menor = nuevoNodo;
                }else{
                    padre.mayor = nuevoNodo;
                }

                actual = null;
                size--;
                return;
            }

            if(actual.mayor != null && actual.menor != null){ // Si tiene dos hijos 
                Nodo temp;
                Nodo padreSucesor = null;

                temp = actual.mayor;
                while(temp.menor != null){
                    padreSucesor = temp;
                    temp = temp.menor;
                }

                if(padreSucesor != null){
                    padreSucesor.menor = temp.mayor;
                }else{
                    actual.mayor = temp.mayor;
                }
                actual.valor = temp.valor;
                temp = null;
            }

            size--;
        }
    }

    public String inOrder(Nodo actual, String stringArbol) {
        if (actual == null) {
            return stringArbol;
        }
        stringArbol = inOrder(actual.menor,stringArbol);
        stringArbol += actual.valor + ",";
        stringArbol = inOrder(actual.mayor,stringArbol);
        
        return stringArbol;
    }

    public String toString(){
        String stringArbol = inOrder(raiz,"");
        stringArbol = stringArbol.substring(0, stringArbol.length()-1);
        return "{" + stringArbol + "}";
    }

    private class ABB_Iterador implements Iterador<T> {
        private Nodo _actual = raiz;
        private int dedo=0;
        private ArrayList<T> lista = new ArrayList<T>();

        public boolean haySiguiente() {            
            return(_actual.menor != null || _actual.mayor != null );
        }

        public ArrayList<T> inOrderT(Nodo actual, ArrayList<T> listaT,int index) {
            if (actual == null) {
                return listaT;
            }
            listaT = inOrderT(actual.menor,listaT,index);
            listaT.add(actual.valor);
            index++;
            listaT = inOrderT(actual.mayor,listaT,index);

            return listaT;
        }
        
        public T siguiente() {
            ArrayList<T> myList = inOrderT(_actual, lista, 0);
            dedo++;
            return myList.get(dedo-1);
        }
    }

    public Iterador<T> iterador() {

        return new ABB_Iterador();
    }

}
