package aed;

import java.util.Scanner;
import java.io.File;
import java.io.PrintStream;

class Archivos {
    float[] leerVector(Scanner entrada, int largo) {

        float[] arrayDeFloat = new float[largo];
        
        for(int i = 0 ; i < largo ; i++){
            arrayDeFloat[i] = entrada.nextFloat();
        }

        return arrayDeFloat;
    }

    float[][] leerMatriz(Scanner entrada, int filas, int columnas) {    

        float[][] matrix = new float[filas][columnas];

        for(int i = 0; i < filas; i++){
            matrix[i] = leerVector(entrada, columnas);
        }
        return matrix;
    }

    void imprimirPiramide(PrintStream salida, int alto) {
        
        for(int i = 0; i < alto ; i++){
            salida.println(" ".repeat(alto-(i+1))
            + "*".repeat((2*(i+1))-1) 
            + " ".repeat(alto-(i+1)));
        }
    }
}
