package aed;

class Funciones {
    int cuadrado(int x) {
        return x*x;
    }

    double distancia(double x, double y) {
        return Math.sqrt(x*x + y*y);
    }

    boolean esPar(int n) {
        return n % 2 == 0;
    }

    boolean esBisiesto(int n) {
        return ((n % 4 == 0) && (n % 100 != 0)) || (n % 400 == 0);
    }

    int factorialIterativo(int n) {
        if (n == 0){
            return 1;
        }
        int res = 1;

        for(int i = n; i != 1 ; i--){
            res = res * i; 
        }
        return res;
    }

    int factorialRecursivo(int n) {
        if (n == 0){
            return 1;
        }
            return n * factorialRecursivo(n-1);
        
    }

    boolean esPrimo(int n) {
        if (n == 0){
            return false;
        }
        if (n == 1){
            return false;
        }
        if (n == 2){
            return true;
        }
        for(int i = 2; i < n ; i++){
            if(n % i == 0){
                return false;
            }
        }
        return true;
    }

    int sumatoria(int[] numeros) {
        int res = 0;
        for(int x:numeros){
            res += x;
        }
        return res;
    }

    int busqueda(int[] numeros, int buscado) {
        int i = 0;
        for(int x:numeros){
            if(x == buscado){
                return i;
            }
            i++;
        }
        return i;
    }

    boolean tienePrimo(int[] numeros) {
        for(int x:numeros){
            if(esPrimo(x))
            {
                return true;
            }
        }
        return false;
    }

    boolean todosPares(int[] numeros) {
        for(int x:numeros){
            if(x % 2 != 0){
                return false;
            }
        }
        return true;
    }

    boolean esPrefijo(String s1, String s2) {

        if(s1.length() > s2.length()){
            return false;
        }

        for(int i=0;i < s1.length(); i++){
            if(s1.charAt(i) != s2.charAt(i)){
                return false;
            }
        } 
        return true;

    }

    boolean esSufijo(String s1, String s2) {

        if(s2 == s1){
            return true;
        }

        String s3 = "";
        String s4 = "";

        for (int i=0; i<s1.length(); i++){
            char ch= s1.charAt(i);
            s3= ch+s3;
        }
        for (int i=0; i<s2.length(); i++){
            char ch= s2.charAt(i);
            s4= ch+s4;
        }
        
        return (esPrefijo(s3,s4));
    }
}
