package aed;

import java.security.PublicKey;
import java.util.*;


public class InternetToolkit {
    public InternetToolkit() {
    }

    // Ejercicio 1 ///////////////////////////////////////////////////////////////////

    public Fragment[] tcpReorder(Fragment[] fragments) { // En el caso que este desordenado es O(n²) pero como suele estar ordenado es O(n)
        
        for(int i = 1; i < fragments.length ; i++){ // Hago insertion sort
            Fragment num = fragments[i];    
            int j = i -1;

            while( j >= 0 && fragments[j].compareTo(num) > 0){ // busco un fragmento fuera de lugar
                fragments[j+1] = fragments[j]; // muevo el valor del fragmento uno abajo
                j--;
            }
            fragments[j+1] = num; // pongo el valor nuevo del fragmento
        } 

        return fragments;
    }

    // Ejercicio 2 ///////////////////////////////////////////////////////////////////

    public class ComparadorRouter implements Comparator<Router> {
        @Override
        public int compare(Router p1, Router p2) {
            if(p1.getTrafico() == p2.getTrafico()){
                return 0;
            }
            else if(p1.getTrafico() > p2.getTrafico())
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }
    }


    public Router[] kTopRouters(Router[] routers, int k, int umbral) {

        PriorityQueue<Router> heapRouters = new PriorityQueue<Router>(routers.length, new ComparadorRouter().reversed()); // O(1) Creo Heap de Routers que campara el trafico
        
        for(int i = 0; i < routers.length; i++){ // O(n) Añado todos los routers al heap
            heapRouters.add(routers[i]);
        }

        LinkedList<Router> nuevoRouters = new LinkedList<Router>();  // O(1) Creo la lista para guardar los routers que me piden

        for(int i = 0; i < k ; i++){ // O(k log n) busco al menos k routers que su trafico que supere el umbral 
            if(heapRouters.peek().getTrafico() >= umbral){
                nuevoRouters.add(heapRouters.poll());
            }
        }

        Router[] resultado = new Router[nuevoRouters.size()]; // O(k) Creo el array para guardar los elementos de la LE

        for(int i = 0; i < resultado.length; i++){ // O(k)
            resultado[i] = nuevoRouters.poll();
        }

        return resultado;

    }

    // Ejercicio 3 ///////////////////////////////////////////////////////////////////

    public class CustomComparator implements Comparator<String> {
        @Override
        public int compare(String a, String b) {

            String[] octetsA = a.split("\\.");
            String[] octetsB = b.split("\\.");
 
            if (Arrays.equals(octetsA, octetsB)) {
                return 0;
            } else if (Integer.parseInt(octetsA[0]) > Integer.parseInt(octetsB[0])) {
                return 1;
            } else if (Integer.parseInt(octetsA[0]) < Integer.parseInt(octetsB[0])) {
                return -1;
            } else if (Integer.parseInt(octetsA[1]) > Integer.parseInt(octetsB[1])) {
                return 1;
            } else if (Integer.parseInt(octetsA[1]) < Integer.parseInt(octetsB[1])) {
                return -1;
            } else if (Integer.parseInt(octetsA[2]) > Integer.parseInt(octetsB[2])) {
                return 1;
            } else if (Integer.parseInt(octetsA[2]) < Integer.parseInt(octetsB[2])) {
                return -1;
            } else if (Integer.parseInt(octetsA[3]) > Integer.parseInt(octetsB[3])) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    public IPv4Address[] sortIPv4(String[] ipv4) {
        Arrays.sort(ipv4, new CustomComparator());

        IPv4Address[] ipSorteadas = new IPv4Address[ipv4.length];
        for(int i =0; i < ipv4.length; i++){
            ipSorteadas[i] = new IPv4Address(ipv4[i]);
        }

        return ipSorteadas;
        }
}

